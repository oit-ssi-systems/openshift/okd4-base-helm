# Misc. Systems-y stuff to put in the cluster

## Setup

Grant additional SCC for Namespaces

```
oc create ns ssi-systems-harbor
oc adm policy add-scc-to-user privileged -z default -n ss-systems-harbor
oc adm policy add-scc-to-user anyuid -z default -n ss-systems-harbor
oc create ns cert-manager
oc adm policy add-scc-to-user privileged -z default -n cert-manager
```

Sync everything up with

```
helmfile apply
```

## Post Setup

Create locksmith issuer

```
kubectl apply -f yaml.d/locksmith.yaml
```

## Automated Certs on Ingresses and Routes

Annotate your ingress with the following to get an ACME cert from cert-manager

```
...
  annotations:
    kubernetes.io/tls-acme: "true"
    cert-manager.io/cluster-issuer: "locksmith"
    acme.cert-manager.io/http01-edit-in-place: "true"
...
```

### Troubleshooting Certs

Helpful commands

Certificate-manager interacts with ACME/Locksmith using
[CRDs](https://kubernetes.io/docs/concepts/extend-kubernetes/api-extension/custom-resources/).
To see all of these in the cluster, use the following:

```
kubectl get crd | grep cert-manager
```

Check what's up with a Certificate

```
kubectl get certificate

kubectl describe certificate
```

The commands above should display the start of a chain of requests that can be
followed to see the status of a request

Force a certificate renewal

```
kubectl krew install cert-manager
kubectl cert-manager renew $CERT_NAME
```
